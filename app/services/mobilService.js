const carRepository = require("../repositories/mobilRepository");

module.exports = {
    async findAll() {
        const data = await carRepository.findAll();
        const count = data.length;
        return {
            data: data,
            total: count
        };
    },

    findOne(id) {
        return carRepository.find(id)
    },

    async create(requestBody) {
        const name = requestBody.body.name;
        const photo = requestBody.body.photo;
        const price = requestBody.body.price;
        const size = requestBody.body.size;

        const data = carRepository.create({ name, photo, price, size});
        return {
            data
        }
        

    },

    delete(id) {
        return carRepository.delete(id);
    },

    update(id, requestBody, paranoid=true) {
        return carRepository.update(id, requestBody, paranoid);
    },
}