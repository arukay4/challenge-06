const userRepository = require("../repositories/userRepository");
const encrypt = require("../controllers/auth")
const jwt = require("jsonwebtoken");

module.exports = {
    async create(requestBody) {
        const email = requestBody.body.email;
        const pass = requestBody.body.password;
        
    
        const password = await encrypt.encryptPassword(pass);

        const data = userRepository.create({ email, password});
        return {
            data
        }
    },

    async checkUser(req, res) {
          let checkIfExist = await userRepository.findOne({
            where: { email: req.body.email },
          });
      
          if (checkIfExist) {
            let idGenerator = checkIfExist.id;
            let validation = await userRepository.findOne({
              where: { id: idGenerator },
            });
            let checkPassword = await encrypt.decryptPass(
              validation.password,
              req.body.password
            );
            if (req.body.email === validation.email && checkPassword) {
              let user = {
                id: validation.id,
                email: validation.email,
              };
              let token = jwt.sign(user, "s3cr3t");
             
              return {
                data: checkIfExist,
                token,
                message: 'User found'
            }
            } 
          } 
        } 

}