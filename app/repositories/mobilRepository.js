const { Car } = require("../../models")

module.exports = {
    findAll() {
        return Car.findAll();
    },

    find(id) {
        return Car.findByPk(id);
    },
    create(requestBody) {
        return Car.create(requestBody);
    },

    delete(id) {
        return Car.destroy({ where: { id: id } });
    },

    update(id, updateArgs, paranoid=true) {
        return Car.update(updateArgs, {
            where: {
                id,
            },
            paranoid
        });
    }
}