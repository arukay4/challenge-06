const carService = require("../services/carService");

async function findAll(req, res) {
    try {
        const data = await carService.list()
        return res.status(200).json({
            success: true,
            error: false,
            message: " Data successfully populated",
            data: data
        });
    } catch (error) {
        return res.status(400).json({
            success: false,
            error: true,
            data: null,
            message: error
        });
    }
}

async function create(req, res) {
        try {
            const carcreate = await carService.create(req);
            const { data } = carcreate;

        if(!data){
            res.status(500).json({
                success: false,
                error: true,
                data,
            });
        }
        res.status(200).json({
            success: true,
            error: false,
            message: "Mobil berhasil ditambahkan!",
            data
        });
    } catch (err) {
        res.status(500).json({
            success: false,
            error: true,
            data: null,
            message: "Mobil gagal ditambahkan."
        });
        console.log(err)
    };
    }

module.exports = { create, findAll};