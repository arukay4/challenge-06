const express = require("express");
const app = express();
const controller = require("./app/controllers");

// Define PORT
const { PORT = 8000 } = process.env;

app.use(express.json());

// Register
app.post("/api/v1/register", controller.userController.register);
app.post("/api/v1/login", controller.userController.login)
app.post("/api/v1/carpost", controller.carController.create);

app.listen(PORT, () => {
  console.log("Server sudah berjalan di : ", PORT);
});
